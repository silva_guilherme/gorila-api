package br.com.gorila.core.security.authorizationserver;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gorila.domain.model.Jogador;
import br.com.gorila.domain.repository.JogadorRepository;

@Service
public class JpaUserDetailsService implements UserDetailsService {

	@Autowired
	private JogadorRepository jogadorRepository;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Jogador jogador = jogadorRepository.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException("Jogador não encontrado com e-mail informado"));

		return new AuthUser(jogador, new ArrayList<>());
	}

}
