package br.com.gorila.core.security.authorizationserver;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.com.gorila.domain.model.Jogador;
import lombok.Getter;

@Getter
public class AuthUser extends User {

	private static final long serialVersionUID = 1L;

	private Long userId;
	private String fullName;

	public AuthUser(Jogador jogador, Collection<? extends GrantedAuthority> authorities) {
		super(jogador.getEmail(), jogador.getSenha(), authorities);

		this.userId = jogador.getId();
		this.fullName = jogador.getNome();
	}

}
