package br.com.gorila.core.modelmapper;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.gorila.api.v1.model.input.AcaoInput;
import br.com.gorila.api.v1.model.input.FaseInput;
import br.com.gorila.api.v1.model.input.FaseSemListaInput;
import br.com.gorila.domain.model.Acao;
import br.com.gorila.domain.model.Fase;

@Configuration
public class ModelMapperConfig {

	@Bean
	public ModelMapper modelMapper() {
		var modelMapper = new ModelMapper();

		modelMapper.createTypeMap(FaseInput.class, Fase.class).addMappings(mapper -> mapper.skip(Fase::setId));
		modelMapper.createTypeMap(FaseSemListaInput.class, Fase.class).addMappings(mapper -> mapper.skip(Fase::setId));
		modelMapper.createTypeMap(AcaoInput.class, Acao.class).addMappings(mapper -> mapper.skip(Acao::setId));

		return modelMapper;
	}
}
