/**
 * 
 */
package br.com.gorila.domain.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gorila.api.v1.properties.MetricasPerfilAcoesProperties;
import br.com.gorila.domain.exception.AcaoPontuacaoMaiorPermitidoException;
import br.com.gorila.domain.exception.JogoNaoEncontradoException;
import br.com.gorila.domain.model.Jogo;
import br.com.gorila.domain.repository.JogoRepository;

/**
 * @author guilh
 *
 */
@Service
public class CadastroJogoService {

	@Autowired
	private JogoRepository jogoRepository;

	@Autowired
	private MetricasPerfilAcoesProperties metricasPerfilAcoesProperties;

	public Jogo buscarOuFalhar(Long idJogo) {
		return jogoRepository.findById(idJogo).orElseThrow(() -> new JogoNaoEncontradoException(idJogo));
	}

	@Transactional
	public Jogo salvar(Jogo jogo) {
		jogoRepository.detach(jogo);

		validarFases(jogo);
		validarAcoes(jogo);

		return jogoRepository.save(jogo);
	}

	private void validarAcoes(Jogo jogo) {
		jogo.getFases().forEach(fase -> {
			fase.getAcoes().forEach(acao -> {
				acao.setFase(fase);
				if (acao.getPontuacao()
						.compareTo(Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoPorAcao())) > 0) {
					throw new AcaoPontuacaoMaiorPermitidoException(acao.getComentario(),
							Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoPorAcao()));
				}
			});
		});

	}

	private void validarFases(Jogo jogo) {
		jogo.getFases().forEach(fase -> fase.setJogo(jogo));

	}

	@Transactional
	public void alterarValorTotalComeco(Long jogoId, BigDecimal valorTotalComeco) {
		Jogo jogo = buscarOuFalhar(jogoId);
		jogo.setValorTotalComeco(valorTotalComeco);
	}

}
