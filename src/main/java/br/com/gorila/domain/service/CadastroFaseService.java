/**
 * 
 */
package br.com.gorila.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gorila.api.v1.properties.MetricasPerfilAcoesProperties;
import br.com.gorila.domain.exception.AcaoPontuacaoMaiorPermitidoException;
import br.com.gorila.domain.exception.FaseNaoEncontradoException;
import br.com.gorila.domain.model.Fase;
import br.com.gorila.domain.model.Jogo;
import br.com.gorila.domain.repository.FaseRepository;

/**
 * @author guilh
 *
 */
@Service
public class CadastroFaseService {

	@Autowired
	private FaseRepository faseRepository;

	@Autowired
	private CadastroJogoService cadastroJogo;

	@Autowired
	private MetricasPerfilAcoesProperties metricasPerfilAcoesProperties;

	public Fase buscarOuFalhar(Long idFase) {
		return faseRepository.findById(idFase).orElseThrow(() -> new FaseNaoEncontradoException(idFase));
	}

	@Transactional
	public Fase salvar(Fase novaFase, Long jogoId) {
		Jogo jogo = cadastroJogo.buscarOuFalhar(jogoId);
		faseRepository.detach(novaFase);

		novaFase.setJogo(jogo);
		novaFase.getAcoes().forEach(acao -> {
			acao.setFase(novaFase);
			if (acao.getPontuacao()
					.compareTo(Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoPorAcao())) > 0) {
				throw new AcaoPontuacaoMaiorPermitidoException(acao.getComentario(),
						Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoPorAcao()));
			}
		});

		return faseRepository.save(novaFase);
	}

	@Transactional
	public void deletarFase(Long jogoId, Long faseId) {
		cadastroJogo.buscarOuFalhar(jogoId);
		buscarOuFalhar(faseId);

		faseRepository.deleteById(faseId);
	}

	public Fase alterar(Fase alterarFase, Long faseId) {
		Fase fase = buscarOuFalhar(faseId);
		alterarFase.setAcoes(fase.getAcoes());
		alterarFase.setId(faseId);
		alterarFase.setJogo(fase.getJogo());
		faseRepository.detach(alterarFase);
		return faseRepository.save(alterarFase);
	}

}
