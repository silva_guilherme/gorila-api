/**
 * 
 */
package br.com.gorila.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gorila.api.v1.properties.MetricasPerfilAcoesProperties;
import br.com.gorila.domain.exception.AcaoNaoEncontradoException;
import br.com.gorila.domain.exception.AcaoPontuacaoMaiorPermitidoException;
import br.com.gorila.domain.model.Acao;
import br.com.gorila.domain.model.Fase;
import br.com.gorila.domain.repository.AcaoRepository;

/**
 * @author guilh
 *
 */
@Service
public class CadastroAcaoService {

	@Autowired
	private AcaoRepository acaoRepository;

	@Autowired
	private CadastroFaseService cadastroFase;

	@Autowired
	private MetricasPerfilAcoesProperties metricasPerfilAcoesProperties;

	public Acao buscarOuFalhar(Long idAcao) {
		return acaoRepository.findById(idAcao).orElseThrow(() -> new AcaoNaoEncontradoException(idAcao));
	}

	@Transactional
	public Acao salvar(Acao novaAcao, Long faseId) {
		Fase fase = cadastroFase.buscarOuFalhar(faseId);
		verificaPontuacaoAcao(novaAcao);

		acaoRepository.detach(novaAcao);

		novaAcao.setFase(fase);

		return acaoRepository.save(novaAcao);
	}

	private void verificaPontuacaoAcao(Acao novaAcao) {
		if (novaAcao.getPontuacao()
				.compareTo(Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoPorAcao())) > 0) {
			throw new AcaoPontuacaoMaiorPermitidoException(novaAcao.getComentario(),
					Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoPorAcao()));
		}
	}

	@Transactional
	public void deletarFase(Long faseId, Long acaoId) {
		cadastroFase.buscarOuFalhar(faseId);
		buscarOuFalhar(acaoId);

		acaoRepository.deleteById(acaoId);
	}

	@Transactional
	public Acao alterar(Acao alterarAcao, Long acaoId) {
		Acao acao = buscarOuFalhar(acaoId);
		verificaPontuacaoAcao(alterarAcao);

		acaoRepository.detach(alterarAcao);
		alterarAcao.setFase(acao.getFase());
		alterarAcao.setId(acaoId);
		return acaoRepository.save(alterarAcao);
	}

}
