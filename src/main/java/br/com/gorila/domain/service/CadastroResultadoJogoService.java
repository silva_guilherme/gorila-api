package br.com.gorila.domain.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gorila.api.v1.model.input.ResultadoJogoInput;
import br.com.gorila.api.v1.properties.MetricasPerfilAcoesProperties;
import br.com.gorila.domain.model.Acao;
import br.com.gorila.domain.model.ResultadoJogo;
import br.com.gorila.domain.repository.ResultadoJogoRepository;

@Service
public class CadastroResultadoJogoService {

	@Autowired
	private ResultadoJogoRepository resultadoJogoRepository;

	@Autowired
	private MetricasPerfilAcoesProperties metricasPerfilAcoesProperties;

	@Autowired
	private CadastroAcaoService cadastroAcao;

	@Autowired
	private CadastroJogoService cadastroJogo;

	@Autowired
	private CadastroJogadorService cadastroJogador;

	@Transactional
	public ResultadoJogo calcularPerfilResultadoFinal(ResultadoJogoInput resultadoJogoInput) {

		final ResultadoJogo resultadoJogo = new ResultadoJogo();
		resultadoJogo.setJogador(cadastroJogador.buscarOuFalhar(resultadoJogoInput.getJogador().getId()));
		resultadoJogo.setJogo(cadastroJogo.buscarOuFalhar(resultadoJogoInput.getJogo().getId()));

		final List<Acao> acoes = new ArrayList<>();

		resultadoJogoInput.getAcoes().stream().forEach(acaoId -> {
			acoes.add(cadastroAcao.buscarOuFalhar(acaoId.getId()));
		});

		final Double mediaPontuacao = acoes.stream().mapToInt(Acao::getPontuacao).average().orElse(Double.NaN);
		BigDecimal resultadoSomaPorcentagem = new BigDecimal(0);
		for (Acao acao : acoes) {
			resultadoSomaPorcentagem = resultadoSomaPorcentagem.add(acao.getPorcentagem().divide(new BigDecimal(100)));
		}
		resultadoSomaPorcentagem = resultadoSomaPorcentagem.add(new BigDecimal(1))
				.multiply(resultadoJogo.getJogo().getValorTotalComeco());
		resultadoJogo.setValorTotalCalculado(resultadoSomaPorcentagem);

		if (mediaPontuacao.intValue() > Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoArrojado())) {
			resultadoJogo.setPerfil("ARROJADO");
		} else if (mediaPontuacao.intValue() > Integer.valueOf(metricasPerfilAcoesProperties.getPontuacaoModerado())) {
			resultadoJogo.setPerfil("MODERADO");
		} else {
			resultadoJogo.setPerfil("CONSERVADOR");
		}

		resultadoJogoRepository.detach(resultadoJogo);
		return resultadoJogoRepository.save(resultadoJogo);
	}

}
