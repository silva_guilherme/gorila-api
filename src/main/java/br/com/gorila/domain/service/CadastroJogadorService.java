package br.com.gorila.domain.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gorila.domain.exception.JogadorNaoEncontradoException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.Jogador;
import br.com.gorila.domain.repository.JogadorRepository;

@Service
public class CadastroJogadorService {

	@Autowired
	private JogadorRepository jogadorRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Transactional
	public Jogador salvar(Jogador jogador) {
		jogadorRepository.detach(jogador);

		Optional<Jogador> jogadorExistente = jogadorRepository.findByEmail(jogador.getEmail());

		if (jogadorExistente.isPresent() && !jogadorExistente.get().equals(jogador)) {
			throw new NegocioException(
					String.format("Já existe um jogador cadastrado com o e-mail %s", jogador.getEmail()));
		}

		if (jogador.isNovo()) {
			jogador.setSenha(passwordEncoder.encode(jogador.getSenha()));
		}

		return jogadorRepository.save(jogador);
	}

	@Transactional
	public void alterarSenha(Long jogadorId, String senhaAtual, String novaSenha) {
		Jogador jogador = buscarOuFalhar(jogadorId);

		if (!passwordEncoder.matches(senhaAtual, jogador.getSenha())) {
			throw new NegocioException("Senha atual informada não coincide com a senha do jogador.");
		}

		jogador.setSenha(passwordEncoder.encode(novaSenha));
	}

	public Jogador buscarOuFalhar(Long jogadorId) {
		return jogadorRepository.findById(jogadorId).orElseThrow(() -> new JogadorNaoEncontradoException(jogadorId));
	}
}
