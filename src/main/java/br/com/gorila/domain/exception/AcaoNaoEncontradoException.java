package br.com.gorila.domain.exception;

public class AcaoNaoEncontradoException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public AcaoNaoEncontradoException(Long idAcao) {
		super(String.format("Não existe uma ação com ID %s", idAcao));
	}

}
