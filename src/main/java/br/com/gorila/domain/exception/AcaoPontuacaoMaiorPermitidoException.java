package br.com.gorila.domain.exception;

public class AcaoPontuacaoMaiorPermitidoException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public AcaoPontuacaoMaiorPermitidoException(String descricao, Integer valorPermitido) {
		super(String.format("Ação com a seguinte descrição `%s` possui pontuação acima do permitido que é %S",
				descricao, valorPermitido));
	}

}
