package br.com.gorila.domain.exception;

public class JogoNaoEncontradoException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public JogoNaoEncontradoException(Long idJogo) {
		super(String.format("Não existe um jogo com ID %s", idJogo));
	}

}
