package br.com.gorila.domain.exception;

public class FaseNaoEncontradoException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public FaseNaoEncontradoException(Long idFase) {
		super(String.format("Não existe uma fase com ID %s", idFase));
	}

}
