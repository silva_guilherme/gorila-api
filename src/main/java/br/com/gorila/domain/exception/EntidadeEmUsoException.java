/**
 * 
 */
package br.com.gorila.domain.exception;

/**
 * @author guilh
 *
 */
public class EntidadeEmUsoException extends NegocioException {

	private static final long serialVersionUID = 1L;

	public EntidadeEmUsoException(String mensagem) {
		super(mensagem);
	}

}
