/**
 * 
 */
package br.com.gorila.domain.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author guilh
 *
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Fase {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer ordem;

	private String comentario;

	private Boolean faseFinal;

	@OneToMany(mappedBy = "fase", cascade = CascadeType.ALL)
	private List<Acao> acoes = new ArrayList<>();

	@ManyToOne
	@JoinColumn(nullable = false)
	private Jogo jogo;

}
