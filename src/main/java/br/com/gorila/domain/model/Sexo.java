package br.com.gorila.domain.model;

public enum Sexo {

	FEMININO("Feminino"), MASCULINO("Confirmado");

	Sexo(String descricao) {
		this.descricao = descricao;
	}

	private String descricao;

	public String getDescricao() {
		return this.descricao;
	}
}
