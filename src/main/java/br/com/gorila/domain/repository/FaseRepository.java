package br.com.gorila.domain.repository;

import org.springframework.stereotype.Repository;

import br.com.gorila.domain.model.Fase;

@Repository
public interface FaseRepository extends CustomJpaRepository<Fase, Long> {

}
