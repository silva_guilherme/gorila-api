package br.com.gorila.domain.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.gorila.domain.model.Jogador;

@Repository
public interface JogadorRepository extends CustomJpaRepository<Jogador, Long> {

	Optional<Jogador> findByEmail(String email);
}
