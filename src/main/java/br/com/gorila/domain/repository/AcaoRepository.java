package br.com.gorila.domain.repository;

import org.springframework.stereotype.Repository;

import br.com.gorila.domain.model.Acao;

@Repository
public interface AcaoRepository extends CustomJpaRepository<Acao, Long> {

}
