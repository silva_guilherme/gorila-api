package br.com.gorila.domain.repository;

import org.springframework.stereotype.Repository;

import br.com.gorila.domain.model.ResultadoJogo;

@Repository
public interface ResultadoJogoRepository extends CustomJpaRepository<ResultadoJogo, Long> {

}
