package br.com.gorila.domain.repository;

import org.springframework.stereotype.Repository;

import br.com.gorila.domain.model.Jogo;

@Repository
public interface JogoRepository extends CustomJpaRepository<Jogo, Long> {

}
