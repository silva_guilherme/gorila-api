package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.model.input.AcaoInput;
import br.com.gorila.domain.model.Acao;

@Component
public class AcaoInputDisassembler {

	@Autowired
	private ModelMapper modelMapper;

	public Acao toDomainObject(AcaoInput acaoInput) {
		return modelMapper.map(acaoInput, Acao.class);
	}

	public void copyToDomainObject(AcaoInput acaoInput, Acao acao) {
		modelMapper.map(acaoInput, acao);
	}
}
