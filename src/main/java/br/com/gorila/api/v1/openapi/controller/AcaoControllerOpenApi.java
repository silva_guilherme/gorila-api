package br.com.gorila.api.v1.openapi.controller;

import br.com.gorila.api.exceptionhandler.Problem;
import br.com.gorila.api.v1.model.AcaoModel;
import br.com.gorila.api.v1.model.input.AcaoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Ações")
public interface AcaoControllerOpenApi {

	@ApiOperation("Alterar uma Ação na Fase")
	@ApiResponses({ @ApiResponse(code = 204, message = "Alteração realizada com sucesso"),
			@ApiResponse(code = 404, message = "Ação não encontrado", response = Problem.class) })
	AcaoModel alterar(@ApiParam(value = "ID da Ação", example = "1", required = true) Long acaoId,
			@ApiParam(name = "corpo", value = "Representação de uma nova acao", required = true) AcaoInput acaoInput);
}
