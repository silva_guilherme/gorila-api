package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.controller.ResultadoJogoController;
import br.com.gorila.api.v1.model.ResultadoJogoModel;
import br.com.gorila.domain.model.ResultadoJogo;

@Component
public class ResultadoJogoModelAssembler
		extends RepresentationModelAssemblerSupport<ResultadoJogo, ResultadoJogoModel> {

	public ResultadoJogoModelAssembler() {
		super(ResultadoJogoController.class, ResultadoJogoModel.class);
	}

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public ResultadoJogoModel toModel(ResultadoJogo jogo) {
		ResultadoJogoModel jogoModel = createModelWithId(jogo.getId(), jogo);
		modelMapper.map(jogo, jogoModel);

		return jogoModel;
	}
}
