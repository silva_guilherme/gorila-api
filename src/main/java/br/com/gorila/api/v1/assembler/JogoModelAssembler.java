package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.controller.JogoController;
import br.com.gorila.api.v1.model.JogoModel;
import br.com.gorila.domain.model.Jogo;

@Component
public class JogoModelAssembler extends RepresentationModelAssemblerSupport<Jogo, JogoModel> {

	public JogoModelAssembler() {
		super(JogoController.class, JogoModel.class);
	}

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public JogoModel toModel(Jogo jogo) {
		JogoModel jogoModel = createModelWithId(jogo.getId(), jogo);
		modelMapper.map(jogo, jogoModel);

		return jogoModel;
	}
}
