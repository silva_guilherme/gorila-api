/**
 * 
 */
package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.controller.FaseController;
import br.com.gorila.api.v1.model.FaseModel;
import br.com.gorila.domain.model.Fase;

/**
 * @author guilh
 *
 */
@Component
public class FaseModelAssembler extends RepresentationModelAssemblerSupport<Fase, FaseModel> {

	@Autowired
	private ModelMapper modelMapper;

	public FaseModelAssembler() {
		super(FaseController.class, FaseModel.class);
	}

	@Override
	public FaseModel toModel(Fase fase) {
		FaseModel faseModel = createModelWithId(fase.getId(), fase, fase.getJogo().getId());
		modelMapper.map(fase, faseModel);

		return faseModel;
	}

	@Override
	public CollectionModel<FaseModel> toCollectionModel(Iterable<? extends Fase> entities) {
		CollectionModel<FaseModel> collectionModel = super.toCollectionModel(entities);

		return collectionModel;
	}
}
