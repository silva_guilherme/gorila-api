package br.com.gorila.api.v1.model.input;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FaseSemListaInput {

	@ApiModelProperty(example = "1", required = true)
	@NotNull
	private Integer ordem;

	@ApiModelProperty(example = "Investir dinheiro em ações, FII ou tesouro?", required = true)
	@NotBlank
	private String comentario;

	@ApiModelProperty(example = "false", required = true)
	@NotNull
	private Boolean faseFinal;

}
