package br.com.gorila.api.v1.openapi.controller;

import br.com.gorila.api.v1.model.ResultadoJogoModel;
import br.com.gorila.api.v1.model.input.ResultadoJogoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Resultados do Jogos")
public interface ResultadoJogoControllerOpenApi {

	@ApiOperation("Carcular perfil do jogador em um jogo")
	@ApiResponses({ @ApiResponse(code = 201, message = "Perfil Calculado"), })
	ResultadoJogoModel calculaPerfil(
			@ApiParam(name = "corpo", value = "Representação de um novo cálculo para um jogador", required = true) ResultadoJogoInput resultadoJogoInput);

}
