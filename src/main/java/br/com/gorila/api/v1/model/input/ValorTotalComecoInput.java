package br.com.gorila.api.v1.model.input;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ValorTotalComecoInput {

	@ApiModelProperty(example = "100.58", required = true)
	@NotNull
	private BigDecimal valorTotalComeco;

}
