package br.com.gorila.api.v1.openapi.controller;

import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import br.com.gorila.api.exceptionhandler.Problem;
import br.com.gorila.api.v1.model.AcaoModel;
import br.com.gorila.api.v1.model.input.AcaoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Fases")
public interface AcaoFaseControllerOpenApi {

	@ApiOperation("Lista as ações associados a uma fase")
	@ApiResponses({ @ApiResponse(code = 404, message = "Ação não encontrado", response = Problem.class) })
	CollectionModel<AcaoModel> listar(@ApiParam(value = "ID da Fase", example = "1", required = true) Long faseId);

	@ApiOperation("Deletar uma ação da fase")
	@ApiResponses({ @ApiResponse(code = 204, message = "Remoção realizada com sucesso"),
			@ApiResponse(code = 404, message = "Fase ou Ação não encontrado", response = Problem.class) })
	ResponseEntity<Void> deletar(@ApiParam(value = "ID da Fase", example = "1", required = true) Long faseId,

			@ApiParam(value = "ID Acao", example = "1", required = true) Long acaoId);

	@ApiOperation("Adicionar uma Ação na Fase")
	@ApiResponses({ @ApiResponse(code = 204, message = "Associação realizada com sucesso"),
			@ApiResponse(code = 404, message = "Fase ou Ação não encontrado", response = Problem.class) })
	AcaoModel adiconar(@ApiParam(value = "ID da Fase", example = "1", required = true) Long faseId,
			@ApiParam(name = "corpo", value = "Representação de uma nova acao", required = true) AcaoInput acaoInput);

}
