package br.com.gorila.api.v1.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.JogadorInputDisassembler;
import br.com.gorila.api.v1.assembler.JogadorModelAssembler;
import br.com.gorila.api.v1.model.JogadorModel;
import br.com.gorila.api.v1.model.input.JogadorComSenhaInput;
import br.com.gorila.api.v1.model.input.JogadorInput;
import br.com.gorila.api.v1.model.input.SenhaInput;
import br.com.gorila.api.v1.openapi.controller.JogadorControllerOpenApi;
import br.com.gorila.domain.model.Jogador;
import br.com.gorila.domain.repository.JogadorRepository;
import br.com.gorila.domain.service.CadastroJogadorService;

@RestController
@RequestMapping(path = "/v1/jogadores", produces = MediaType.APPLICATION_JSON_VALUE)
public class JogadorController implements JogadorControllerOpenApi {

	@Autowired
	private JogadorRepository jogadorRepository;

	@Autowired
	private CadastroJogadorService cadastroJogador;

	@Autowired
	private JogadorModelAssembler jogadorModelAssembler;

	@Autowired
	private JogadorInputDisassembler jogadorInputDisassembler;

	@Override
	@GetMapping
	public CollectionModel<JogadorModel> listar() {
		List<Jogador> todosJogadores = jogadorRepository.findAll();

		return jogadorModelAssembler.toCollectionModel(todosJogadores);
	}

	@Override
	@GetMapping("/{jogadorId}")
	public JogadorModel buscar(@PathVariable Long jogadorId) {
		Jogador jogador = cadastroJogador.buscarOuFalhar(jogadorId);

		return jogadorModelAssembler.toModel(jogador);
	}

	@Override
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public JogadorModel adicionar(@RequestBody @Valid JogadorComSenhaInput jogadorInput) {
		Jogador jogador = jogadorInputDisassembler.toDomainObject(jogadorInput);
		jogador = cadastroJogador.salvar(jogador);

		return jogadorModelAssembler.toModel(jogador);
	}

	@Override
	@PutMapping("/{jogadorId}")
	public JogadorModel atualizar(@PathVariable Long jogadorId, @RequestBody @Valid JogadorInput jogadorInput) {
		Jogador jogadorAtual = cadastroJogador.buscarOuFalhar(jogadorId);
		jogadorInputDisassembler.copyToDomainObject(jogadorInput, jogadorAtual);
		jogadorAtual = cadastroJogador.salvar(jogadorAtual);

		return jogadorModelAssembler.toModel(jogadorAtual);
	}

	@Override
	@PutMapping("/{jogadorId}/senha")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void alterarSenha(@PathVariable Long jogadorId, @RequestBody @Valid SenhaInput senha) {
		cadastroJogador.alterarSenha(jogadorId, senha.getSenhaAtual(), senha.getNovaSenha());
	}
}
