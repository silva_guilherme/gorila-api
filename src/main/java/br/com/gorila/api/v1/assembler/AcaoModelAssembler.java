/**
 * 
 */
package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.controller.AcaoFaseController;
import br.com.gorila.api.v1.model.AcaoModel;
import br.com.gorila.domain.model.Acao;

/**
 * @author guilh
 *
 */
@Component
public class AcaoModelAssembler extends RepresentationModelAssemblerSupport<Acao, AcaoModel> {

	@Autowired
	private ModelMapper modelMapper;

	public AcaoModelAssembler() {
		super(AcaoFaseController.class, AcaoModel.class);
	}

	@Override
	public AcaoModel toModel(Acao acao) {
		AcaoModel acaoModel = createModelWithId(acao.getId(), acao, acao.getFase().getId());
		modelMapper.map(acao, acaoModel);

		return acaoModel;
	}

	@Override
	public CollectionModel<AcaoModel> toCollectionModel(Iterable<? extends Acao> entities) {
		CollectionModel<AcaoModel> collectionModel = super.toCollectionModel(entities);

		return collectionModel;
	}
}
