package br.com.gorila.api.v1.model;

import java.math.BigDecimal;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Relation(collectionRelation = "resultadosJogos")
@Setter
@Getter
public class ResultadoJogoModel extends RepresentationModel<ResultadoJogoModel> {

	@ApiModelProperty(example = "1200.00")
	private BigDecimal valorTotalCalculado;

	@ApiModelProperty(example = "MODERADO")
	private String perfil;

	private JogadorModel jogador;

	private JogoModel jogo;

}
