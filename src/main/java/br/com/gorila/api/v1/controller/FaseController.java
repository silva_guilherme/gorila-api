package br.com.gorila.api.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.FaseModelAssembler;
import br.com.gorila.api.v1.assembler.FaseSemListaInputDisassembler;
import br.com.gorila.api.v1.model.FaseModel;
import br.com.gorila.api.v1.model.input.FaseSemListaInput;
import br.com.gorila.api.v1.openapi.controller.FaseControllerOpenApi;
import br.com.gorila.domain.exception.EntidadeNaoEncontradaException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.Fase;
import br.com.gorila.domain.service.CadastroFaseService;

@RestController
@RequestMapping(path = "/v1/fases", produces = MediaType.APPLICATION_JSON_VALUE)
public class FaseController implements FaseControllerOpenApi {

	@Autowired
	private CadastroFaseService cadastroFase;

	@Autowired
	private FaseModelAssembler faseModelAssembler;

	@Autowired
	private FaseSemListaInputDisassembler faseInputDisassembler;

	@Override
	@PutMapping(path = "/{faseId}")
	public FaseModel alterar(@PathVariable Long faseId, @Valid @RequestBody FaseSemListaInput acaoInput) {
		try {
			Fase alterarFase = faseInputDisassembler.toDomainObject(acaoInput);

			alterarFase = cadastroFase.alterar(alterarFase, faseId);

			return faseModelAssembler.toModel(alterarFase);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
