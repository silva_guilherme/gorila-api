package br.com.gorila.api.v1.openapi.controller;

import org.springframework.hateoas.CollectionModel;

import br.com.gorila.api.exceptionhandler.Problem;
import br.com.gorila.api.v1.model.JogadorModel;
import br.com.gorila.api.v1.model.input.JogadorComSenhaInput;
import br.com.gorila.api.v1.model.input.JogadorInput;
import br.com.gorila.api.v1.model.input.SenhaInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Jogadores")
public interface JogadorControllerOpenApi {

	@ApiOperation("Lista os Jogadores")
	CollectionModel<JogadorModel> listar();

	@ApiOperation("Busca um jogador por ID")
	@ApiResponses({ @ApiResponse(code = 400, message = "ID do jogador inválido", response = Problem.class),
			@ApiResponse(code = 404, message = "Jogador não encontrado", response = Problem.class) })
	JogadorModel buscar(@ApiParam(value = "ID do jogador", example = "1", required = true) Long jogadorId);

	@ApiOperation("Cadastra um jogador")
	@ApiResponses({ @ApiResponse(code = 201, message = "Jogador cadastrado"), })
	JogadorModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo jogador", required = true) JogadorComSenhaInput jogadorInput);

	@ApiOperation("Atualiza um jogador por ID")
	@ApiResponses({ @ApiResponse(code = 200, message = "Jogador atualizado"),
			@ApiResponse(code = 404, message = "Jogador não encontrado", response = Problem.class) })
	JogadorModel atualizar(@ApiParam(value = "ID do jogador", example = "1", required = true) Long jogadorId,

			@ApiParam(name = "corpo", value = "Representação de um jogador com os novos dados", required = true) JogadorInput jogadorInput);

	@ApiOperation("Atualiza a senha de um jogador")
	@ApiResponses({ @ApiResponse(code = 204, message = "Senha alterada com sucesso"),
			@ApiResponse(code = 404, message = "Jogador não encontrado", response = Problem.class) })
	void alterarSenha(@ApiParam(value = "ID do jogador", example = "1", required = true) Long jogadorId,

			@ApiParam(name = "corpo", value = "Representação de uma nova senha", required = true) SenhaInput senha);
}
