/**
 * 
 */
package br.com.gorila.api.v1.model;

import java.math.BigDecimal;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author guilh
 *
 */
@Relation(collectionRelation = "acoes")
@Setter
@Getter
public class AcaoModel extends RepresentationModel<AcaoModel> {

	@ApiModelProperty(example = "1")
	private Long id;

	@ApiModelProperty(example = "Investir dinheiro em ações da PETR4?")
	private String comentario;

	@ApiModelProperty(example = "5.90")
	private BigDecimal porcentagem;

	@ApiModelProperty(example = "10")
	private Integer pontuacao;

	@ApiModelProperty(example = "ACAO")
	private String tipoInvestimento;

}
