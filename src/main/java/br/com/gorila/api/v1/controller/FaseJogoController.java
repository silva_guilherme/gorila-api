package br.com.gorila.api.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.FaseInputDisassembler;
import br.com.gorila.api.v1.assembler.FaseModelAssembler;
import br.com.gorila.api.v1.model.FaseModel;
import br.com.gorila.api.v1.model.input.FaseInput;
import br.com.gorila.api.v1.openapi.controller.FaseJogoControllerOpenApi;
import br.com.gorila.domain.exception.EntidadeNaoEncontradaException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.Fase;
import br.com.gorila.domain.model.Jogo;
import br.com.gorila.domain.service.CadastroFaseService;
import br.com.gorila.domain.service.CadastroJogoService;

@RestController
@RequestMapping(path = "/v1/jogos/{jogoId}/fases", produces = MediaType.APPLICATION_JSON_VALUE)
public class FaseJogoController implements FaseJogoControllerOpenApi {

	@Autowired
	private CadastroJogoService cadastroJogo;

	@Autowired
	private CadastroFaseService cadastroFase;

	@Autowired
	private FaseModelAssembler faseModelAssembler;

	@Autowired
	private FaseInputDisassembler faseInputDisassembler;

	@Override
	@GetMapping
	public CollectionModel<FaseModel> listar(@PathVariable Long jogoId) {
		Jogo jogo = cadastroJogo.buscarOuFalhar(jogoId);
		CollectionModel<FaseModel> fasesModel = faseModelAssembler.toCollectionModel(jogo.getFases()).removeLinks();
		return fasesModel;

	}

	@Override
	@DeleteMapping("/{faseId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> deletar(@PathVariable Long jogoId, @PathVariable Long faseId) {
		cadastroFase.deletarFase(jogoId, faseId);
		return ResponseEntity.noContent().build();
	}

	@Override
	@PutMapping
	public FaseModel adiconar(@PathVariable Long jogoId, @Valid @RequestBody FaseInput faseInput) {
		try {
			Fase novaFase = faseInputDisassembler.toDomainObject(faseInput);

			novaFase = cadastroFase.salvar(novaFase, jogoId);

			return faseModelAssembler.toModel(novaFase);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
