package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.model.input.JogadorInput;
import br.com.gorila.domain.model.Jogador;

@Component
public class JogadorInputDisassembler {

	@Autowired
	private ModelMapper modelMapper;

	public Jogador toDomainObject(JogadorInput jogadorInput) {
		return modelMapper.map(jogadorInput, Jogador.class);
	}

	public void copyToDomainObject(JogadorInput jogadorInput, Jogador jogador) {
		modelMapper.map(jogadorInput, jogador);
	}
}
