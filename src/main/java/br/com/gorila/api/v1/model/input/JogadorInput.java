package br.com.gorila.api.v1.model.input;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JogadorInput {

	@ApiModelProperty(example = "João da Silva", required = true)
	@NotBlank
	private String nome;

	@ApiModelProperty(example = "joao.ger@gorila.com.br", required = true)
	@NotBlank
	@Email
	private String email;

	@ApiModelProperty(example = "18", required = true)
	private Integer idade;

	@ApiModelProperty(example = "Masculino", required = true)
	private String sexo;
}
