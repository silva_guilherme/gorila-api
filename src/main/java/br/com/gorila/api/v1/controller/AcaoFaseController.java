package br.com.gorila.api.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.AcaoInputDisassembler;
import br.com.gorila.api.v1.assembler.AcaoModelAssembler;
import br.com.gorila.api.v1.model.AcaoModel;
import br.com.gorila.api.v1.model.input.AcaoInput;
import br.com.gorila.api.v1.openapi.controller.AcaoFaseControllerOpenApi;
import br.com.gorila.domain.exception.EntidadeNaoEncontradaException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.Acao;
import br.com.gorila.domain.model.Fase;
import br.com.gorila.domain.service.CadastroAcaoService;
import br.com.gorila.domain.service.CadastroFaseService;

@RestController
@RequestMapping(path = "/v1/fases/{faseId}/acoes", produces = MediaType.APPLICATION_JSON_VALUE)
public class AcaoFaseController implements AcaoFaseControllerOpenApi {

	@Autowired
	private CadastroAcaoService cadastroAcao;

	@Autowired
	private AcaoModelAssembler acaoModelAssembler;

	@Autowired
	private AcaoInputDisassembler acaoInputDisassembler;

	@Autowired
	private CadastroFaseService cadastroFase;

	@Override
	@GetMapping
	public CollectionModel<AcaoModel> listar(@PathVariable Long faseId) {
		Fase fase = cadastroFase.buscarOuFalhar(faseId);
		CollectionModel<AcaoModel> acoesModel = acaoModelAssembler.toCollectionModel(fase.getAcoes()).removeLinks();
		return acoesModel;
	}

	@Override
	@DeleteMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> deletar(@PathVariable Long faseId, @PathVariable Long acaoId) {
		cadastroAcao.deletarFase(faseId, acaoId);
		return ResponseEntity.noContent().build();
	}

	@Override
	@PutMapping
	public AcaoModel adiconar(@PathVariable Long faseId, @Valid @RequestBody AcaoInput acaoInput) {
		try {
			Acao novaAcao = acaoInputDisassembler.toDomainObject(acaoInput);

			novaAcao = cadastroAcao.salvar(novaAcao, faseId);

			return acaoModelAssembler.toModel(novaAcao);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
