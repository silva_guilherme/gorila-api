package br.com.gorila.api.v1.openapi.controller;

import br.com.gorila.api.exceptionhandler.Problem;
import br.com.gorila.api.v1.model.FaseModel;
import br.com.gorila.api.v1.model.input.FaseSemListaInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Fases")
public interface FaseControllerOpenApi {

	@ApiOperation("Alterar uma Fase do Jogo")
	@ApiResponses({ @ApiResponse(code = 204, message = "Alteração realizada com sucesso"),
			@ApiResponse(code = 404, message = "Fase não encontrado", response = Problem.class) })
	FaseModel alterar(@ApiParam(value = "ID da Fase", example = "1", required = true) Long faseId,
			@ApiParam(name = "corpo", value = "Representação de uma nova acao", required = true) FaseSemListaInput faseInput);
}
