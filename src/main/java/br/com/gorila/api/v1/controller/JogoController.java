/**
 * 
 */
package br.com.gorila.api.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.JogoInputDisassembler;
import br.com.gorila.api.v1.assembler.JogoModelAssembler;
import br.com.gorila.api.v1.model.JogoModel;
import br.com.gorila.api.v1.model.input.JogoInput;
import br.com.gorila.api.v1.model.input.ValorTotalComecoInput;
import br.com.gorila.api.v1.openapi.controller.JogoControllerOpenApi;
import br.com.gorila.domain.exception.EntidadeNaoEncontradaException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.Jogo;
import br.com.gorila.domain.service.CadastroJogoService;

/**
 * @author guilh
 *
 */
@RestController
@RequestMapping(path = "/v1/jogos", produces = MediaType.APPLICATION_JSON_VALUE)
public class JogoController implements JogoControllerOpenApi {

	@Autowired
	private CadastroJogoService cadastroJogo;

	@Autowired
	private JogoModelAssembler jogoModelAssembler;

	@Autowired
	private JogoInputDisassembler jogoInputDisassembler;

	@Override
	@GetMapping("/{idJogo}")
	public JogoModel buscar(@PathVariable Long idJogo) {
		Jogo jogo = cadastroJogo.buscarOuFalhar(idJogo);

		return jogoModelAssembler.toModel(jogo);
	}

	@Override
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public JogoModel adicionar(@Valid @RequestBody JogoInput jogoInput) {
		try {
			Jogo novoJogo = jogoInputDisassembler.toDomainObject(jogoInput);

			novoJogo = cadastroJogo.salvar(novoJogo);

			return jogoModelAssembler.toModel(novoJogo);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	@Override
	@PutMapping("/{jogoId}/valor")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void alterarValorTotalComeco(@PathVariable Long jogoId,
			@RequestBody @Valid ValorTotalComecoInput valorTotalComeco) {
		cadastroJogo.alterarValorTotalComeco(jogoId, valorTotalComeco.getValorTotalComeco());
	}

}
