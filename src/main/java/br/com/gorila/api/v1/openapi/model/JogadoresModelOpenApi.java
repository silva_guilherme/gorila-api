/**
 * 
 */
package br.com.gorila.api.v1.openapi.model;

import java.util.List;

import org.springframework.hateoas.Links;

import br.com.gorila.api.v1.model.JogadorModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author guilh
 *
 */
@ApiModel("JogadoresModel")
@Data
public class JogadoresModelOpenApi {

	private JogadoresEmbeddedModelOpenApi _embedded;
	private Links _links;

	@ApiModel("JogadoresEmbeddedModel")
	@Data
	public class JogadoresEmbeddedModelOpenApi {

		private List<JogadorModel> jogadores;

	}
}
