package br.com.gorila.api.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.ResultadoJogoModelAssembler;
import br.com.gorila.api.v1.model.ResultadoJogoModel;
import br.com.gorila.api.v1.model.input.ResultadoJogoInput;
import br.com.gorila.api.v1.openapi.controller.ResultadoJogoControllerOpenApi;
import br.com.gorila.domain.exception.EntidadeNaoEncontradaException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.ResultadoJogo;
import br.com.gorila.domain.service.CadastroResultadoJogoService;

@RestController
@RequestMapping(path = "/v1/resultados-jogos", produces = MediaType.APPLICATION_JSON_VALUE)
public class ResultadoJogoController implements ResultadoJogoControllerOpenApi {

	@Autowired
	private ResultadoJogoModelAssembler resultadoJogoModelAssembler;

	@Autowired
	private CadastroResultadoJogoService cadastroResultadoJogo;

	@Override
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResultadoJogoModel calculaPerfil(@Valid @RequestBody ResultadoJogoInput resultadoJogoInput) {
		try {
			ResultadoJogo novoResultadoJogo = cadastroResultadoJogo.calcularPerfilResultadoFinal(resultadoJogoInput);

			return resultadoJogoModelAssembler.toModel(novoResultadoJogo);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
