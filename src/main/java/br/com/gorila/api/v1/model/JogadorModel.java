package br.com.gorila.api.v1.model;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Relation(collectionRelation = "jogadores")
@Setter
@Getter
public class JogadorModel extends RepresentationModel<JogadorModel> {

	@ApiModelProperty(example = "1")
	private Long id;

	@ApiModelProperty(example = "João da Silva")
	private String nome;

	@ApiModelProperty(example = "joao.ger@gorila.com.br")
	private String email;

	@ApiModelProperty(example = "18")
	private Integer idade;

	@ApiModelProperty(example = "Masculino")
	private String sexo;
}
