package br.com.gorila.api.v1.openapi.controller;

import br.com.gorila.api.exceptionhandler.Problem;
import br.com.gorila.api.v1.model.JogoModel;
import br.com.gorila.api.v1.model.input.JogoInput;
import br.com.gorila.api.v1.model.input.ValorTotalComecoInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Jogos")
public interface JogoControllerOpenApi {

	@ApiOperation("Busca um jogo por ID")
	@ApiResponses({ @ApiResponse(code = 404, message = "Jogo não encontrado", response = Problem.class) })
	JogoModel buscar(@ApiParam(value = "ID de um jogo", example = "1", required = true) Long idJogo);

	@ApiOperation("Registra um jogo")
	@ApiResponses({ @ApiResponse(code = 201, message = "Jogo registrado"), })
	JogoModel adicionar(
			@ApiParam(name = "corpo", value = "Representação de um novo jogo", required = true) JogoInput pedidoInput);

	@ApiOperation("Atualiza o valor total do começo de um jogo")
	@ApiResponses({ @ApiResponse(code = 204, message = "Valor total do começo alterada com sucesso"),
			@ApiResponse(code = 404, message = "Jogo não encontrado", response = Problem.class) })
	void alterarValorTotalComeco(@ApiParam(value = "ID do jogo", example = "1", required = true) Long jogoId,

			@ApiParam(name = "corpo", value = "Representação de um novo valor total começo", required = true) ValorTotalComecoInput valorTotalComeco);
}
