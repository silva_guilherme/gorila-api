package br.com.gorila.api.v1.model.input;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResultadoJogoInput {

	@Valid
	@NotNull
	private JogadorIdInput jogador;

	@Valid
	@NotNull
	private JogoIdInput jogo;

	@Valid
	@Size(min = 1)
	@NotNull
	private List<AcaoIdInput> acoes;
}
