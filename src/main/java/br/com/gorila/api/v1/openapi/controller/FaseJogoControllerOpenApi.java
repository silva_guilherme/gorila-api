package br.com.gorila.api.v1.openapi.controller;

import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;

import br.com.gorila.api.exceptionhandler.Problem;
import br.com.gorila.api.v1.model.FaseModel;
import br.com.gorila.api.v1.model.input.FaseInput;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(tags = "Jogos")
public interface FaseJogoControllerOpenApi {

	@ApiOperation("Lista as fases associados a um jogo")
	@ApiResponses({ @ApiResponse(code = 404, message = "Fase não encontrado", response = Problem.class) })
	CollectionModel<FaseModel> listar(@ApiParam(value = "ID do jogo", example = "1", required = true) Long jogoId);

	@ApiOperation("Deletar uma fase do jogo")
	@ApiResponses({ @ApiResponse(code = 204, message = "Remoção realizada com sucesso"),
			@ApiResponse(code = 404, message = "Fase ou Jogo não encontrado", response = Problem.class) })
	ResponseEntity<Void> deletar(@ApiParam(value = "ID do jogo", example = "1", required = true) Long jogoId,

			@ApiParam(value = "ID fase", example = "1", required = true) Long faseId);

	@ApiOperation("Adicionar uma Fase com o jogo")
	@ApiResponses({ @ApiResponse(code = 204, message = "Associação realizada com sucesso"),
			@ApiResponse(code = 404, message = "Fase ou Jogo não encontrado", response = Problem.class) })
	FaseModel adiconar(@ApiParam(value = "ID do jogo", example = "1", required = true) Long jogoId,
			@ApiParam(name = "corpo", value = "Representação de uma nova fase", required = true) FaseInput fase);
}
