/**
 * 
 */
package br.com.gorila.api.v1.openapi.model;

import java.util.List;

import org.springframework.hateoas.Links;

import br.com.gorila.api.v1.model.FaseModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author guilh
 *
 */
@ApiModel("FasesModel")
@Data
public class FasesModelOpenApi {

	private FasesEmbeddedModelOpenApi _embedded;
	private Links _links;

	@ApiModel("FasesEmbeddedModel")
	@Data
	public class FasesEmbeddedModelOpenApi {

		private List<FaseModel> fases;

	}
}
