package br.com.gorila.api.v1.model;

import java.util.List;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Relation(collectionRelation = "fases")
@Setter
@Getter
public class FaseModel extends RepresentationModel<FaseModel> {

	@ApiModelProperty(example = "1")
	private Long id;

	@ApiModelProperty(example = "1")
	private Integer ordem;

	@ApiModelProperty(example = "Investir dinheiro em ações, FII ou tesouro?")
	private String comentario;

	@ApiModelProperty(example = "false")
	private Boolean faseFinal;

	private List<AcaoModel> acoes;

}
