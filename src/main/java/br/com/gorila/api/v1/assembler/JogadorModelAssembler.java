package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.controller.JogadorController;
import br.com.gorila.api.v1.model.JogadorModel;
import br.com.gorila.domain.model.Jogador;

@Component
public class JogadorModelAssembler extends RepresentationModelAssemblerSupport<Jogador, JogadorModel> {

	@Autowired
	private ModelMapper modelMapper;

	public JogadorModelAssembler() {
		super(JogadorController.class, JogadorModel.class);
	}

	@Override
	public JogadorModel toModel(Jogador jogador) {
		JogadorModel jogadorModel = createModelWithId(jogador.getId(), jogador);
		modelMapper.map(jogador, jogadorModel);

		return jogadorModel;
	}

	@Override
	public CollectionModel<JogadorModel> toCollectionModel(Iterable<? extends Jogador> entities) {
		CollectionModel<JogadorModel> collectionModel = super.toCollectionModel(entities);

		return collectionModel;
	}
}
