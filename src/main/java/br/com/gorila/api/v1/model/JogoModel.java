package br.com.gorila.api.v1.model;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Relation(collectionRelation = "jogos")
@Setter
@Getter
public class JogoModel extends RepresentationModel<JogoModel> {

	@ApiModelProperty(example = "1")
	private Long id;

	@ApiModelProperty(example = "308.90")
	private BigDecimal valorTotalComeco;

	private List<FaseModel> fases;
}
