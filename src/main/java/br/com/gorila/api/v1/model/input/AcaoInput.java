package br.com.gorila.api.v1.model.input;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AcaoInput {

	@ApiModelProperty(example = "Investir dinheiro em ações da PETR4?", required = true)
	@NotBlank
	private String comentario;

	@ApiModelProperty(example = "5.90", required = true)
	@NotNull
	private BigDecimal porcentagem;

	@ApiModelProperty(example = "10", required = true)
	@NotNull
	private Integer pontuacao;

	@ApiModelProperty(example = "ACAO", required = true)
	@NotBlank
	private String tipoInvestimento;
}
