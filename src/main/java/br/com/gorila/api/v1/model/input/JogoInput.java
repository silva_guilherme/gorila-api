package br.com.gorila.api.v1.model.input;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JogoInput {

	@ApiModelProperty(example = "308.90", required = true)
	@NotNull
	private BigDecimal valorTotalComeco;

	@Valid
	@Size(min = 1)
	@NotNull
	private List<FaseInput> fases;
}
