package br.com.gorila.api.v1.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gorila.api.v1.assembler.AcaoInputDisassembler;
import br.com.gorila.api.v1.assembler.AcaoModelAssembler;
import br.com.gorila.api.v1.model.AcaoModel;
import br.com.gorila.api.v1.model.input.AcaoInput;
import br.com.gorila.api.v1.openapi.controller.AcaoControllerOpenApi;
import br.com.gorila.domain.exception.EntidadeNaoEncontradaException;
import br.com.gorila.domain.exception.NegocioException;
import br.com.gorila.domain.model.Acao;
import br.com.gorila.domain.service.CadastroAcaoService;

@RestController
@RequestMapping(path = "/v1/acoes", produces = MediaType.APPLICATION_JSON_VALUE)
public class AcaoController implements AcaoControllerOpenApi {

	@Autowired
	private CadastroAcaoService cadastroAcao;

	@Autowired
	private AcaoModelAssembler acaoModelAssembler;

	@Autowired
	private AcaoInputDisassembler acaoInputDisassembler;

	@Override
	@PutMapping(path = "/{acaoId}")
	public AcaoModel alterar(@PathVariable Long acaoId, @Valid @RequestBody AcaoInput acaoInput) {
		try {
			Acao alterarAcao = acaoInputDisassembler.toDomainObject(acaoInput);

			alterarAcao = cadastroAcao.alterar(alterarAcao, acaoId);

			return acaoModelAssembler.toModel(alterarAcao);
		} catch (EntidadeNaoEncontradaException e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

}
