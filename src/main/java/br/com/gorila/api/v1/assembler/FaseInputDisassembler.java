package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.model.input.FaseInput;
import br.com.gorila.domain.model.Fase;

@Component
public class FaseInputDisassembler {

	@Autowired
	private ModelMapper modelMapper;

	public Fase toDomainObject(FaseInput faseInput) {
		return modelMapper.map(faseInput, Fase.class);
	}

	public void copyToDomainObject(FaseInput faseInput, Fase fase) {
		modelMapper.map(faseInput, fase);
	}
}
