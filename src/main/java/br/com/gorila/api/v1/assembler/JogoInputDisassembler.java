package br.com.gorila.api.v1.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.gorila.api.v1.model.input.JogoInput;
import br.com.gorila.domain.model.Jogo;

@Component
public class JogoInputDisassembler {

	@Autowired
	private ModelMapper modelMapper;

	public Jogo toDomainObject(JogoInput jogoInput) {
		return modelMapper.map(jogoInput, Jogo.class);
	}

	public void copyToDomainObject(JogoInput jogoInput, Jogo jogo) {
		modelMapper.map(jogoInput, jogo);
	}
}
