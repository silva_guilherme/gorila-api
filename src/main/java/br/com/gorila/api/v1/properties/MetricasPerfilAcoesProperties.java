package br.com.gorila.api.v1.properties;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Validated
@Component
@ConfigurationProperties("gorila.metricas.perfil.acoes")
public class MetricasPerfilAcoesProperties {

	@NotNull
	private String pontuacaoPorAcao;

	@NotNull
	private String pontuacaoConservador;

	@NotNull
	private String pontuacaoModerado;

	@NotNull
	private String pontuacaoArrojado;

}
