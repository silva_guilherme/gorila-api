/**
 * 
 */
package br.com.gorila.api.v1.openapi.model;

import java.util.List;

import org.springframework.hateoas.Links;

import br.com.gorila.api.v1.model.AcaoModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author guilh
 *
 */
@ApiModel("AcoesModel")
@Data
public class AcoesModelOpenApi {

	private AcoesEmbeddedModelOpenApi _embedded;
	private Links _links;

	@ApiModel("AcoesEmbeddedModel")
	@Data
	public class AcoesEmbeddedModelOpenApi {

		private List<AcaoModel> acoes;

	}
}
