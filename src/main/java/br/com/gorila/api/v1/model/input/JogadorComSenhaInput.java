package br.com.gorila.api.v1.model.input;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JogadorComSenhaInput extends JogadorInput {

	@ApiModelProperty(example = "123", required = true)
	@NotBlank
	private String senha;
}
