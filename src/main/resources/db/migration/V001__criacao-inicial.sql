create table jogador (
	id bigint not null auto_increment,
	nome varchar(80) not null,
	email varchar(255) not null,
	senha varchar(255) not null,
	data_cadastro datetime not null,
	
	primary key (id)
) engine=InnoDB default charset=utf8;

create table jogo (
	id bigint not null auto_increment,
	valor_total_comeco decimal(10,2) not null,
	
	primary key (id)
) engine=InnoDB default charset=utf8;

create table fase (
	id bigint not null auto_increment,
	ordem smallint(6) not null,
	comentario varchar(255) not null,
	fase_final tinyint(1) not null,
	jogo_id bigint not null,
	
	primary key (id),
	constraint fk_fase_jogo foreign key (jogo_id) references jogo (id)
) engine=InnoDB default charset=utf8;

create table acao (
	id bigint not null auto_increment,
	comentario varchar(255) not null,
	porcentagem decimal(10,2) not null,
	pontuacao smallint(6) not null,
	tipo_investimento varchar(50) not null,
	fase_id bigint not null,
	
	primary key (id),
	constraint fk_acao_fase foreign key (fase_id) references fase (id)
) engine=InnoDB default charset=utf8;
