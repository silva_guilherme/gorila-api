create table resultado_jogo (
	id bigint not null auto_increment,
	jogador_id bigint not null,
	jogo_id bigint not null,
	valor_total_calculado decimal(10,2) not null,
	perfil varchar(80) not null,
	
	primary key (id),
	constraint fk_resultado_jg_jogador foreign key (jogador_id) references jogador (id),
	constraint fk_resultado_jg_jogo foreign key (jogo_id) references jogo (id)
) engine=InnoDB default charset=utf8;